var app = angular.module('app', ['ngRoute']);

// Definindo Rotas
app.config(function($routeProvider){

    $routeProvider
    .when("/", {
        templateUrl : 'pages/home.html',
        controller  : 'HomeController',
        reloadOnSearch: false,
        cache: false
    })
    .when("/preview/:id", {
        templateUrl : 'pages/preview.html',
        controller  : 'PreviewController'
    })
    .otherwise({redirectTo: '/'});
});


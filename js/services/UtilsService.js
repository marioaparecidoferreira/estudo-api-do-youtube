app.service('UtilsService', function($http) {
    var _self = {};

     _self.successCallback = function (response) {
        var ret = response.data;

        if (ret.code != 200) {
            ret.type = "successCallback";
            ret.method = response.config.method;
            ret.url = response.config.url;
        };

        return ret;
    };

    _self.errorCallback = function (response) {
        var ret = {
            type: "errorCallback",
            code: response.status,
            message: 'Ocorreu um erro!',
            method: response.config.method,
            data: response.config.data,
            url: response.config.url
        };

        return ret;
    };

    var key = 'AIzaSyCEJD7Wwlb1Rs_1eaVfJXFtvdAZ2QnY9Yw'

    _self.getYoutubeByTerm = function (term) {
        var url = 'https://www.googleapis.com/youtube/v3/search?part=snippet&q=' + term + '&key=' + key;
            config = {};

        return $http.get(url, config).then(_self.successCallback, _self.errorCallback);
    };

    _self.getYoutubeByTermNextPage = function (term, nextPageToken) {
        var url = 'https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=10&order=viewCount&pageToken=' + nextPageToken + '&q=' + term + '&type=video&key=' + key;
            config = {};

        return $http.get(url, config).then(_self.successCallback, _self.errorCallback);
    };

    _self.getYoutubeVideo = function (videoId) {
        var url = 'https://www.googleapis.com/youtube/v3/videos?id='+ videoId +'&part=snippet,statistics&key=' + key;
            config = {};

        return $http.get(url, config).then(_self.successCallback, _self.errorCallback);
    };

    return _self;
});
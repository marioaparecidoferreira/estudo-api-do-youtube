app.controller('PreviewController', function($scope, $rootScope, $routeParams, $sce, UtilsService) {
    $scope.videoId = $routeParams.id;
    $scope.urlVideo = '';

    var getYoutubeVideo = function(videoId) {
        function successCallback(response){
            $scope.video = response.items.shift();
            $scope.urlVideo = setUrlIframe($scope.videoId);
            $rootScope.isResult = true;
        };

        function errorCallback(response){
            alert('Video não pode ser encontrado, por favor faça uma nova busca!');
        };

        if(videoId) {
            UtilsService.getYoutubeVideo(videoId).then(successCallback, successCallback);
        } else {
            alert('Video não pode ser encontrado, por favor faça uma nova busca!');
        };
    },

    setUrlIframe = function(videoId) {
        return $sce.trustAsResourceUrl('https://www.youtube.com/embed/' + videoId);
    };
     
    init = function () {
        getYoutubeVideo($scope.videoId);
    };

    init();
});
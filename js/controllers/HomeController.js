app.controller('HomeController', function($scope, $rootScope, UtilsService) {
    var clearVariables = function(){
        $scope.videos = []; 
        $scope.nextPageToken = null;
        $rootScope.isResult = false;
    };

    $scope.getYoutubeByTerm = function(term, nextPageToken) {
        function errorCallback(response){
            clearVariables();
            alert('Sem resultado de busca!');
        };

        function successCallback(response){
            if(!!response.items ? !!response.items.length : false){
                $scope.videos = nextPageToken ? angular.copy($scope.videos.concat(response.items)) : response.items ;
                $scope.nextPageToken = response.nextPageToken;
                $rootScope.isResult = true;
            } else {
                errorCallback(response);
            };
        };

        if(term && nextPageToken) {
           return UtilsService.getYoutubeByTermNextPage(term, nextPageToken).then(successCallback, successCallback);
        } else if(term){
            return UtilsService.getYoutubeByTerm(term).then(successCallback, successCallback);
        } else {
            alert('Preencha os campo de busca.');
        };
    };

    var init = function(){
        clearVariables();
    };

    init();
});